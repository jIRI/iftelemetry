﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IfTelemetry.Models {
	public class IfTelemetryContext : DbContext {
		// You can add custom code to this file. Changes will not be overwritten.
		// 
		// If you want Entity Framework to drop and regenerate your database
		// automatically whenever you change your model schema, please use data migrations.
		// For more information refer to the documentation:
		// http://msdn.microsoft.com/en-us/data/jj591621.aspx

		public IfTelemetryContext()
			: base("name=sqlite") {
		}

		public System.Data.Entity.DbSet<IfTelemetry.Models.Game> Games { get; set; }
		public System.Data.Entity.DbSet<IfTelemetry.Models.Playthrough> Playthroughs { get; set; }
		public System.Data.Entity.DbSet<IfTelemetry.Models.GameAction> GameActions { get; set; }

	}
}
