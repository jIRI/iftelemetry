﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IfTelemetry.Models {
	public class Game {
		[Key]
		public long GameId { get; set; }
		public DateTime Timestamp { get; set; }

		public string GameName { get; set; }
	}
}