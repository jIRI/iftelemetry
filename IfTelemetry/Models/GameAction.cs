﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IfTelemetry.Models {
	public class GameAction {
		[Key]
		public long GameActionId { get; set; }
		public DateTime Timestamp { get; set; }

		public long GameId { get; set; }
		public long PlaythroughId { get; set; }
		public long ActionOrder { get; set; }
		public string ActionType { get; set; }
		public string ActionName { get; set; }
	}
}