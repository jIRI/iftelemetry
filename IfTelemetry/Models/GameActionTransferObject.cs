﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IfTelemetry.Models {
	public class GameActionTransferObject {
		public string Game { get; set; }
		public string Playthrough { get; set; }
		public int Order { get; set; }
		public string Type { get; set; }
		public string Action { get; set; }
	}
}