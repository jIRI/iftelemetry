﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IfTelemetry.Models {
	public class Playthrough {
		[Key]
		public long PlaythroughId { get; set; }
		public DateTime Timestamp { get; set; }

		public long GameId { get; set; }
		public string PlaythroughName { get; set; }
	}
}