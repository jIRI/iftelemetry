﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using IfTelemetry.Models;
using Newtonsoft.Json;
using System.Web.Http.Cors;

namespace IfTelemetry.Controllers {
	[EnableCors(origins: "*", headers: "*", methods: "*")]
	public class GameActionsController : ApiController {
		private IfTelemetryContext db = new IfTelemetryContext();

		// POST: api/GameAction
		[HttpPost]
		public IHttpActionResult PostGameAction(GameActionTransferObject gameActionTO) {
			if(!ModelState.IsValid) {
				return BadRequest(ModelState);
			}

			try {
				var game = db.Games.FirstOrDefault(g => g.GameName == gameActionTO.Game);
				if(game == null) {
					game = new Game {
						Timestamp = DateTime.UtcNow,
						GameName = gameActionTO.Game,
					};
					db.Games.Add(game);
					db.SaveChanges();
				}

				var playthrough = db.Playthroughs.FirstOrDefault(p => p.PlaythroughName == gameActionTO.Playthrough);
				if(playthrough == null) {
					playthrough = new Playthrough {
						Timestamp = DateTime.UtcNow,
						GameId = game.GameId,
						PlaythroughName = gameActionTO.Playthrough,
					};
					db.Playthroughs.Add(playthrough);
					db.SaveChanges();
				}

				var gameAction = new GameAction {
					Timestamp = DateTime.UtcNow,
					GameId = game.GameId,
					PlaythroughId = playthrough.PlaythroughId,
					ActionOrder = gameActionTO.Order,
					ActionType = gameActionTO.Type,
					ActionName = gameActionTO.Action,
				};

				db.GameActions.Add(gameAction);
				db.SaveChanges();

				return Ok("created");
			} catch(Exception ex) {
				return InternalServerError(ex);
			}
		}

		protected override void Dispose(bool disposing) {
			if(disposing) {
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}