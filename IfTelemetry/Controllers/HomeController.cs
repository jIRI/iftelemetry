﻿using IfTelemetry.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IfTelemetry.Controllers {
	public class HomeController : Controller {
		private IfTelemetryContext db = new IfTelemetryContext();
		
		public ActionResult Index() {
			var model = db.Games;

			return View(model);
		}

		public ActionResult About() {
			ViewBag.Message = "Telemetry application for Twine-based IF. Or something.";

			return View();
		}

		public ActionResult Game(int gameId) {
			var game = db.Games.Find(gameId);
			if(game == null) {
				return View("Error");
			}

			ViewBag.GameName = string.Format("{0}", game.GameName);
			var model = db.Playthroughs.Where(p => p.GameId == gameId);
			return View(model);
		}

		public ActionResult Playthrough(int playthroughId) {
			var playthrough = db.Playthroughs.Find(playthroughId);
			if(playthrough == null) {
				return View("Error");
			}

			ViewBag.PlaythroughName = string.Format("{0} - {1}", playthrough.PlaythroughName, playthrough.Timestamp.ToLocalTime());
			var model = db.GameActions.Where(a => a.PlaythroughId == playthroughId).OrderBy(a => a.ActionOrder);
			return View(model);
		}

		protected override void OnException(ExceptionContext filterContext) {
			base.OnException(filterContext);
		}
	}
}